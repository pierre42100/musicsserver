use std::error::Error;
use std::fs;
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex, MutexGuard};

use rand::distributions::Alphanumeric;
use rand::Rng;

use crate::{Config, Music};

pub struct MusicsList {
    list: Arc<Mutex<Vec<Music>>>,
    list_path: PathBuf,
    files_path: PathBuf,
}

impl MusicsList {
    /// Initialize the list of musics
    pub fn load(conf: &Config) -> Result<MusicsList, Box<dyn Error>> {
        let list = MusicsList {
            list: Arc::new(Mutex::new(Vec::new())),
            list_path: conf.list_path(),
            files_path: conf.storage.clone(),
        };

        list.reload()?;

        Ok(list)
    }

    /// Reload the list of serialized musics
    ///
    /// If the musics file does not exists, it
    /// is automatically created
    fn reload(&self) -> Result<(), Box<dyn Error>> {
        let path = Path::new(&self.list_path);

        if !path.exists() {
            eprintln!(
                "File {} does not seems to exists, skipping list refresh...",
                self.list_path.to_string_lossy()
            );
            return Ok(());
        }

        let content = fs::read_to_string(path)?;
        let content = json::parse(&content)?;

        let mut list = vec![];
        for entry in content.members() {
            list.push(Music::from_json(entry)?);
        }

        let mut saved_list = self.list.lock().unwrap();
        *saved_list = list;

        Ok(())
    }

    /// Serialize the list of musics
    pub fn serialize(&self) -> Result<(), Box<dyn Error>> {
        let list = self.list.lock().unwrap();

        let mut data = json::JsonValue::new_array();

        for el in list.iter() {
            data.push(el)?;
        }

        fs::write(Path::new(&self.list_path), data.dump())?;

        Ok(())
    }

    /// Generate a name for a future music
    pub fn generate_file_name(&self) -> String {
        format!(
            "{}.mp3",
            rand::thread_rng()
                .sample_iter(&Alphanumeric)
                .take(20)
                .map(char::from)
                .collect::<String>()
                .as_str()
        )
    }

    /// Get the absolute path to a music file
    pub fn get_music_file_path(&self, m: &Music) -> PathBuf {
        self.files_path.join(&m.file_name)
    }

    /// Get the absolute path to a music file
    pub fn get_music_covers_path(&self, m: &Music) -> PathBuf {
        let covers_path = self.files_path.join("covers");

        if !covers_path.exists() {
            log::info!("Create covers directory...");
            std::fs::create_dir_all(&covers_path).expect("Failed to create covers directory!");
        }

        covers_path.join(&format!("{}.jpg", m.file_name))
    }

    /// Insert a new music into the list
    pub fn insert(&self, music: Music) -> Result<(), Box<dyn Error>> {
        let mut music = music;

        let mut list = self.list.lock().unwrap();

        // Set the ID of music
        music.id = match list.last() {
            Some(m) => m.id + 1,
            None => 1,
        };

        list.push(music);

        drop(list);

        self.serialize()?;

        Ok(())
    }

    /// Get the entire list of musics
    pub fn get_list(&self) -> Result<MutexGuard<Vec<Music>>, Box<dyn Error>> {
        Ok(self.list.lock().unwrap())
    }

    /// Get the absolute path to a music
    pub fn get_music_path_by_id(&self, id: usize) -> Option<PathBuf> {
        let list = self.list.lock().unwrap();

        for music in list.iter() {
            if music.id == id {
                return Some(self.get_music_file_path(music));
            }
        }

        None
    }

    /// Get the absolute path to a music cover
    pub fn get_music_cover_path_by_id(&self, id: usize) -> Option<PathBuf> {
        let list = self.list.lock().unwrap();

        for music in list.iter() {
            if music.id == id {
                return Some(self.get_music_covers_path(music));
            }
        }

        None
    }

    /// Remove a music from the list
    pub fn delete_music(&self, id: usize) -> Result<(), Box<dyn Error>> {
        let mut list = self.list.lock().unwrap();

        // Find the music in the list
        let music = list.iter().find(|m| m.id == id).ok_or("Music not found!")?;

        // Delete the associated cover image (if any)
        let cover_path = self.get_music_covers_path(music);
        if cover_path.exists() {
            fs::remove_file(cover_path)?;
        }

        // Delete the associated file (if any)
        let file_path = self.get_music_file_path(music);
        if file_path.exists() {
            fs::remove_file(file_path)?;
        }

        // Remove the music from the list
        let index = list
            .iter()
            .position(|m| m.id == id)
            .ok_or("list.iter().position()")?;
        list.remove(index);

        drop(list);

        // Save the new list of musics
        self.serialize()?;

        Ok(())
    }
}
