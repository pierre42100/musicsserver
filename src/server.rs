use std::borrow::Borrow;
use std::error::Error;
use std::io::Write;
use std::ops::Add;
use std::str::FromStr;
use std::sync::Arc;
use std::time::{Duration, SystemTime};

use actix_multipart::Multipart;
use actix_web::dev::Payload;
use actix_web::error::ErrorBadRequest as F;
use actix_web::error::ErrorUnprocessableEntity as G;
use actix_web::error::{ErrorForbidden, ErrorNotFound};
use actix_web::http::header::{HeaderName, HeaderValue};
use actix_web::http::{header, Method, StatusCode};
use actix_web::web::Data;
use actix_web::{
    delete, get, options, post, web, App, FromRequest, HttpRequest, HttpResponse,
    HttpResponseBuilder, HttpServer, Responder,
};
use futures::future::{ok, Ready};
use futures::StreamExt;

use crate::config::Client;
use crate::webdav::{option_handler, webdav_download, webdav_listing};
use crate::{Config, Music, MusicsList};

/// Server handler
///
/// Upload based on
/// https://github.com/actix/examples/blob/master/multipart/src/main.rs

/// Custom multipart request (to allow to have both at the same time
/// HttpRequest & Multipart
struct MyMultipartRequest {
    multipart: Multipart,
    req: HttpRequest,
}

impl MyMultipartRequest {
    fn new(multipart: Multipart, req: HttpRequest) -> MyMultipartRequest {
        MyMultipartRequest { multipart, req }
    }
}

impl FromRequest for MyMultipartRequest {
    type Error = actix_web::Error;
    type Future = Ready<Result<MyMultipartRequest, actix_web::Error>>;

    #[inline]
    fn from_request(req: &HttpRequest, payload: &mut Payload) -> Self::Future {
        ok(MyMultipartRequest::new(
            Multipart::new(req.headers(), payload.take()),
            req.clone(),
        ))
    }
}

impl Client {
    fn start_response(&self, status: u16) -> HttpResponseBuilder {
        let mut res = HttpResponse::build(StatusCode::from_u16(status).unwrap());
        if let Some(origin) = &self.origin {
            res.append_header(("Access-Control-Allow-Origin", origin.as_str()));
        }
        res
    }

    fn start_response_ok(&self) -> HttpResponseBuilder {
        self.start_response(200)
    }
}

/// Server configuration
pub(crate) struct ServerConf {
    pub(crate) conf: Arc<Config>,
    pub(crate) list: Arc<MusicsList>,
}

/// Get the configuration of the project from a request
fn get_conf(req: &HttpRequest) -> &ServerConf {
    req.app_data::<Data<Arc<ServerConf>>>().unwrap().borrow()
}

/// Check user authentication
pub(crate) fn check_auth(req: &HttpRequest, need_write_access: bool) -> actix_web::Result<Client> {
    // Check for token
    let token = if let Some(query) = req.query_string().split_once("token=").map(|x| x.1) {
        query.as_bytes()
    } else {
        req.headers()
            .get("Token")
            .ok_or_else(|| actix_web::error::ErrorForbidden("Auth token required!"))?
            .as_bytes()
    };

    let conf = &get_conf(req).conf;

    let client = conf
        .find_client_by_token(String::from_utf8_lossy(token).trim())
        .ok_or_else(|| actix_web::error::ErrorForbidden("Invalid auth token!"))?;

    // Check if the user is authorized to perform RW operation
    if need_write_access && client.read_only {
        return Err(ErrorForbidden(
            "You are not allowed to perform this operation!",
        ));
    }

    Ok(client.clone())
}

/// Home route
#[get("/")]
async fn greet(_req: HttpRequest) -> impl Responder {
    "Musics server"
}

#[options("{tail:.*}")]
async fn options(req: HttpRequest) -> HttpResponse {
    if let Some(origin) = req.headers().get("Origin") {
        let conf = get_conf(&req);
        let origin = origin.to_str().unwrap_or("bad");
        if conf.conf.any_client_allow_origin(origin) {
            return HttpResponse::NoContent()
                .append_header(("Access-Control-Allow-Methods", "OPTIONS, GET, HEAD, POST"))
                .append_header(("Access-Control-Allow-Origin", origin))
                .append_header(("Access-Control-Allow-Headers", "Token"))
                .finish();
        }
    }

    HttpResponse::NotFound().body("Unknown host!")
}

#[get("{tail:.*}")]
async fn not_found(_req: HttpRequest) -> actix_web::Result<String> {
    Err(ErrorNotFound("Resource not found"))
}

/// Upload route
#[post("/upload")]
async fn upload(mut req_info: MyMultipartRequest) -> actix_web::Result<String> {
    let conf = get_conf(&req_info.req);

    // Check auth
    check_auth(&req_info.req, true)?;

    let mut music = Music::empty();
    music.id = 1;

    while let Some(item) = req_info.multipart.next().await {
        let mut field = item?;

        let content_type = field.content_disposition();

        let name = content_type
            .get_name()
            .ok_or_else(|| F("Missing field name!"))?
            .to_string();

        // Handle file upload
        if content_type.get_filename().is_some() && name.eq("file") {
            // Generate music path
            let list = &conf.list;
            let file_name = list.generate_file_name();
            music.file_name = file_name.to_string();
            let file_path = list.get_music_file_path(&music);

            // File::create is blocking operation, use threadpool
            log::info!("Will save new music inside {}", file_path.to_string_lossy());
            let mut f = web::block(move || std::fs::File::create(file_path)).await??;

            // Field in turn is stream of *Bytes* object
            while let Some(chunk) = field.next().await {
                let data = chunk.unwrap();
                // filesystem operations are blocking, we have to use threadpool
                f.write_all(&data)?;
            }
        }
        // It is a simple field
        else {
            let mut content = String::new();

            // Get content
            while let Some(chunk) = field.next().await {
                content = format!("{}{}", content, String::from_utf8_lossy(chunk?.as_ref()));
            }

            // Get required information
            match name.as_str() {
                "title" => music.title = content,
                "artist" => music.artist = content,
                _ => (),
            }
        }
    }

    if !music.is_valid() {
        // Remove music file (if already created)
        let path = conf.list.get_music_file_path(&music);
        if !music.file_name.is_empty() && path.is_file() {
            log::info!("Remove temporary music file {:?}", path);
            std::fs::remove_file(path)?;
        }

        return Err(F("Invalid request, some fields are missing!"));
    }

    // Insert a new music
    let list = &get_conf(&req_info.req).list;
    list.insert(music)
        .map_err(|_| actix_web::error::ErrorInternalServerError("Insertion failed"))?;

    Ok("Upload finished".to_string())
}

#[derive(Debug, serde::Serialize)]
struct MusicAPI {
    id: usize,
    artist: String,
    title: String,
}

/// Get the list of musics route
#[get("/list")]
pub async fn get_list(req: HttpRequest) -> actix_web::Result<HttpResponse> {
    let client = check_auth(&req, false)?;

    let list_mgr = &get_conf(&req).list;
    let list = list_mgr
        .get_list()
        .map_err(|_| F("Could not get the list of musics"))?;

    // Turn the list of music into API entries
    let mut array = vec![];
    for music in list.iter() {
        array.push(MusicAPI {
            id: music.id,
            artist: music.artist.to_string(),
            title: music.title.to_string(),
        });
    }

    Ok(client.start_response_ok().json(array))
}

#[derive(serde::Deserialize)]
pub struct RequestId {
    id: usize,
}

/// Download a specific music
#[get("/download/{id}")]
pub async fn download_music(
    req: HttpRequest,
    query: web::Path<RequestId>,
) -> actix_web::Result<HttpResponse> {
    let client = check_auth(&req, false)?;

    // Get the path to the music
    let music_path = &get_conf(&req)
        .list
        .get_music_path_by_id(query.id)
        .ok_or_else(|| F("Music not found!"))?;

    let mut res = actix_files::NamedFile::open(music_path)?.into_response(&req);
    if let Some(origin) = client.origin {
        res.headers_mut().insert(
            HeaderName::from_str("Access-Control-Allow-Origin").unwrap(),
            HeaderValue::from_str(origin.as_str()).unwrap(),
        );
    }

    Ok(res)
}

/// Get a music cover
#[get("/cover/{id}")]
pub async fn cover(
    req: HttpRequest,
    query: web::Path<RequestId>,
) -> actix_web::Result<HttpResponse> {
    let client = check_auth(&req, false)?;

    let list = get_conf(&req).list.clone();

    // Get the path to the cover
    let cover_path = list
        .get_music_cover_path_by_id(query.id)
        .ok_or_else(|| F("Music not found!"))?;

    // If the cover hasn't already been extracted
    if !cover_path.exists() {
        log::info!("Generate cover for music {}", query.id);

        let music_path = list
            .get_music_path_by_id(query.id)
            .ok_or_else(|| F("Music not found!"))?;

        let cover = id3_image_rs::extract_first_image_as_img(music_path.as_ref())
            .map_err(|_| G("Cover unavailable!"))?;

        cover
            .save(&cover_path)
            .map_err(|_| G("Failed to save cover!"))?;
    }

    let metadata =
        std::fs::metadata(&cover_path).map_err(|_| G("Failed to load cover metadata!"))?;
    let etag = format!("{}-{}", query.id, metadata.len());

    // Check if the browser already knows the etag
    if let Some(c) = req.headers().get(header::IF_NONE_MATCH) {
        if c.to_str().unwrap_or("") == etag {
            return Ok(client.start_response(304).finish());
        }
    }

    // Check if the browser already knows the file by date
    if let Some(c) = req.headers().get(header::IF_MODIFIED_SINCE) {
        let date_str = c.to_str().unwrap_or("");
        if let Ok(date) = httpdate::parse_http_date(date_str) {
            if date.add(Duration::from_secs(1))
                >= metadata.modified().unwrap_or_else(|_| SystemTime::now())
            {
                return Ok(client.start_response(304).finish());
            }
        }
    }

    let cover = std::fs::read(&cover_path).map_err(|_| G("Failed to read cover!"))?;

    Ok(client
        .start_response_ok()
        .content_type("image/jpeg")
        .insert_header(("etag", etag))
        .insert_header((
            "last-modified",
            httpdate::fmt_http_date(metadata.modified().unwrap_or_else(|_| SystemTime::now())),
        ))
        .body(cover))
}

/// Download a specific music
#[delete("/delete/{id}")]
pub async fn delete_music(req: HttpRequest) -> actix_web::Result<String> {
    check_auth(&req, true)?;

    // Get requested ID
    let id: Vec<&str> = req.path().split("delete/").collect();
    let id = id.get(1).ok_or_else(|| F("invalid_id!"))?;
    let id = id.parse::<usize>().map_err(|_| F("invalid_id!(2)"))?;

    // Delete the music
    get_conf(&req)
        .list
        .delete_music(id)
        .map_err(|_| F("could not delete music!"))?;

    Ok("music deleted.".to_string())
}

/// Create a new server instance the server
pub async fn start(conf: &Arc<Config>, list: &Arc<MusicsList>) -> Result<(), Box<dyn Error>> {
    log::info!("Starting to listen on http://{}/", conf.listen_address());

    let server_conf = Data::new(Arc::new(ServerConf {
        conf: conf.clone(),
        list: list.clone(),
    }));

    // Create the server
    HttpServer::new(move || {
        let propfind = Method::from_str("PROPFIND").unwrap();

        App::new()
            .app_data(server_conf.clone())
            .service(greet)
            .service(upload)
            .service(get_list)
            .service(download_music)
            .service(cover)
            .service(delete_music)
            .route("/dav", web::method(Method::OPTIONS).to(option_handler))
            .route("/dav/", web::method(Method::OPTIONS).to(option_handler))
            .route(
                "/dav/{filename}",
                web::method(Method::OPTIONS).to(option_handler),
            )
            .route("/dav", web::method(propfind.clone()).to(webdav_listing))
            .route("/dav/", web::method(propfind.clone()).to(webdav_listing))
            .route("/dav/{filename}", web::method(propfind).to(webdav_listing))
            .route("/dav/{filename}", web::get().to(webdav_download))
            .service(options)
            .service(not_found)
    })
    .bind(conf.listen_address())?
    .run()
    .await?;

    Ok(())
}
