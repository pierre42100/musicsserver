use log::LevelFilter;
use musics_server::{Config, MusicsList, Server};
use std::error::Error;
use std::process::exit;
use std::sync::Arc;

#[actix_rt::main]
async fn main() -> Result<(), Box<dyn Error + 'static>> {
    env_logger::builder()
        .filter_level(LevelFilter::Info)
        .parse_default_env()
        .init();

    log::info!("MusicsServer (c) Pierre HUBERT 2020");

    // Get config from command line
    let args: Vec<String> = std::env::args().collect();

    if args.len() != 2 {
        eprintln!("Usage: {} [path_config]", args[0]);
        return Ok(());
    }

    // Load the configuration
    let conf = Config::load(&args[1]).unwrap_or_else(|error| {
        log::error!("Could not load configuration! ({:?})", error);
        exit(1);
    });

    let conf = Arc::new(conf);

    // Load the list of musics
    let list = MusicsList::load(&conf).unwrap_or_else(|error| {
        log::error!("Could not load musics list! ({:?})", error);
        exit(2);
    });

    // Start the server
    Server::start(&conf, &Arc::new(list)).await?;

    Ok(())
}
