//! # Webdav routes
//!
//! @author Pierre Hubert

use std::borrow::Cow;
use std::path::Path;
use std::str::FromStr;
use std::string::String;
use std::sync::Arc;
use std::time::SystemTime;

use actix_web::error::{ErrorBadRequest as F, ErrorNotFound, ErrorUnauthorized};
use actix_web::{web, HttpRequest, HttpResponse, Responder};
use base64::Engine;
use chrono::{DateTime, Timelike, Utc};
use strong_xml::{XmlRead, XmlWrite};

use crate::Server::ServerConf;

#[derive(XmlWrite, XmlRead, PartialEq, Debug)]
#[xml(tag = "D:multistatus")]
struct Multistatus<'a> {
    #[xml(attr = "xmlns:D")]
    attr1: Cow<'a, str>,
    #[xml(attr = "xmlns:A")]
    attr2: Cow<'a, str>,
    #[xml(child = "D:response")]
    children: Vec<Child<'a>>,
}

impl<'a> Multistatus<'a> {
    pub fn new() -> Self {
        Multistatus {
            attr1: "DAV:".into(),
            attr2: "http://apache.org/dav/props/".into(),
            children: vec![],
        }
    }
}

#[derive(XmlWrite, XmlRead, PartialEq, Debug)]
#[xml(tag = "D:response")]
struct Child<'a> {
    #[xml(child = "D:href")]
    href: Href,
    #[xml(child = "D:propstat")]
    stats: Vec<Propstat<'a>>,
}

#[derive(XmlWrite, XmlRead, PartialEq, Debug)]
#[xml(tag = "D:href")]
struct Href {
    #[xml(text)]
    content: String,
}

impl<'a> Child<'a> {
    pub fn new(
        dav_url: &str,
        filename: &str,
        content_type: &'static str,
        path: &Path,
    ) -> std::io::Result<Self> {
        let metadata = get_file_metadata(path)?;
        Ok(Self {
            href: Href {
                content: format!("{}{}", dav_url, urlencoding::encode(filename)),
            },
            stats: vec![
                Propstat {
                    prop: Prop {
                        creation_date: Some(metadata.creation_date.into()),
                        content_length: Some(metadata.size),
                        content_type: Some(content_type.into()),
                        last_modified: Some(metadata.last_modified.into()),
                        resource_type: Some(ResourceType {
                            directory: match metadata.is_dir {
                                true => Some(Folder {}),
                                false => None,
                            },
                        }),
                        etag: Etag {},
                        executable: Some(match metadata.is_dir {
                            true => "T".to_string(),
                            false => "F".to_string(),
                        }),
                    },
                    status: "HTTP/1.1 200 OK".to_string(),
                },
                Propstat {
                    prop: Prop {
                        creation_date: None,
                        content_length: None,
                        content_type: None,
                        last_modified: None,
                        resource_type: None,
                        etag: Etag {},
                        executable: None,
                    },
                    status: "HTTP/1.1 404 Not Found".to_string(),
                },
            ],
        })
    }
}

#[derive(XmlWrite, XmlRead, PartialEq, Debug)]
#[xml(tag = "D:propstat")]
struct Propstat<'a> {
    #[xml(child = "D:prop")]
    prop: Prop<'a>,

    #[xml(flatten_text = "D:status")]
    status: String,
}

#[derive(XmlWrite, XmlRead, PartialEq, Debug)]
#[xml(tag = "D:prop")]
struct Prop<'a> {
    #[xml(flatten_text = "D:creation_date")]
    creation_date: Option<Cow<'a, str>>,
    #[xml(flatten_text = "D:getcontentlength")]
    content_length: Option<u64>,
    #[xml(flatten_text = "D:getcontenttype")]
    content_type: Option<Cow<'a, str>>,
    #[xml(flatten_text = "D:getlastmodified")]
    last_modified: Option<Cow<'a, str>>,
    #[xml(child = "D:resourcetype")]
    resource_type: Option<ResourceType>,
    #[xml(child = "D:getetag")]
    etag: Etag,
    #[xml(flatten_text = "A:executable")]
    executable: Option<String>,
}

#[derive(XmlWrite, XmlRead, PartialEq, Debug)]
#[xml(tag = "D:resourcetype")]
struct ResourceType {
    #[xml(child = "D:collection")]
    directory: Option<Folder>,
}

#[derive(XmlWrite, XmlRead, PartialEq, Debug)]
#[xml(tag = "D:collection")]
struct Folder {}

#[derive(XmlWrite, XmlRead, PartialEq, Debug)]
#[xml(tag = "D:getetag")]
struct Etag {}

#[derive(serde::Deserialize)]
pub(crate) struct PropfindPath {
    filename: Option<String>,
}

fn format_date(t: SystemTime) -> String {
    let dt: DateTime<Utc> = t.into();
    format!("{:?}", dt.with_nanosecond(0).unwrap())
}

struct FileMetadata {
    creation_date: String,
    last_modified: String,
    size: u64,
    is_dir: bool,
}

/// Get information about a file
fn get_file_metadata(path: &Path) -> std::io::Result<FileMetadata> {
    let metadata = std::fs::metadata(path)?;

    Ok(FileMetadata {
        creation_date: format_date(metadata.created()?),
        last_modified: format_date(metadata.modified()?),
        size: metadata.len(),
        is_dir: metadata.is_dir(),
    })
}

/// Check webdav authentication
fn check_webdav_auth(config: &Arc<ServerConf>, req: &HttpRequest) -> Option<HttpResponse> {
    let token = req
        .headers()
        .get("Authorization")
        // Get header content
        .map(|s| s.to_str().unwrap_or(""))
        // Extract base64-encoded value
        .map(|s| s.trim().rsplit(' ').next().unwrap_or(""))
        // Decode bas64
        .map(|s| {
            String::from_utf8_lossy(
                &base64::engine::general_purpose::STANDARD
                    .decode(s)
                    .unwrap_or_default(),
            )
            .to_string()
        })
        // Extract username
        .map(|s| s.split(':').next().unwrap_or("").to_string())
        // Find client by token
        .map(|s| config.conf.find_client_by_token(s.as_ref()))
        .unwrap_or(None);

    if token.is_none() {
        return Some(
            HttpResponse::Unauthorized()
                .content_type("application/xml; charset=utf-8")
                .append_header(("www-authenticate", "Basic realm=\"Musics server\""))
                .body("<?xml version=\"1.0\" encoding=\"utf-8\"?><d:error xmlns:d=\"DAV:\" />"),
        );
    }

    None
}

pub(crate) async fn option_handler(
    config: web::Data<Arc<ServerConf>>,
    req: HttpRequest,
) -> HttpResponse {
    if let Some(res) = check_webdav_auth(config.as_ref(), &req) {
        return res;
    }

    HttpResponse::NoContent()
        .append_header(("DAV", "1"))
        .append_header(("Allow", "OPTIONS, GET, PROPFIND"))
        .finish()
}

/// Get the listing of a directory
pub(crate) async fn webdav_listing(
    config: web::Data<Arc<ServerConf>>,
    query: web::Path<PropfindPath>,
    req: HttpRequest,
) -> std::io::Result<HttpResponse> {
    // Check auth
    if let Some(res) = check_webdav_auth(&config, &req) {
        return Ok(res);
    }

    let dav_url = format!("{}dav/", config.conf.base_url);

    let depth = req
        .headers()
        .get("Depth")
        .map(|h| h.to_str().ok())
        .unwrap_or(None)
        .unwrap_or("0");

    let mut xml_parent = Multistatus::new();

    // Parent directory
    if query.filename.is_none() && !depth.contains("noroot") {
        xml_parent.children.push(Child::new(
            &dav_url,
            "",
            "application/octet-stream",
            config.conf.storage.as_ref(),
        )?);
    }

    // Process musics list
    if query.filename.is_some() || depth.contains("infinity") || depth.contains('1') {
        let list = config
            .list
            .get_list()
            .map_err(|_| F("music list get fail".to_string()))
            .unwrap();
        for music in list.iter() {
            let pretty_file_name = music.pretty_filename();

            // Check if music has to be skipped
            if let Some(name) = &query.filename {
                if !name.eq(&pretty_file_name) {
                    continue;
                }
            }

            xml_parent.children.push(Child::new(
                &dav_url,
                &pretty_file_name,
                "audio/mpeg",
                config.list.get_music_file_path(music).as_ref(),
            )?);
        }
    }

    // Check if nothing was returned
    if xml_parent.children.is_empty() {
        return Ok(HttpResponse::NotFound().json("Nothing found!"));
    }

    // Construct & return response
    let xml = format!(
        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n{}",
        xml_parent.to_string().unwrap()
    );
    Ok(HttpResponse::MultiStatus()
        .content_type("text/xml; charset=utf-8")
        .append_header(("DAV", "1"))
        .body(xml))
}

#[derive(serde::Deserialize)]
pub(crate) struct DownloadPath {
    filename: String,
}

/// Download webdav file
pub(crate) async fn webdav_download(
    config: web::Data<Arc<ServerConf>>,
    query: web::Path<DownloadPath>,
    req: HttpRequest,
) -> actix_web::Result<impl Responder> {
    // Check auth
    if let Some(_res) = check_webdav_auth(config.as_ref(), &req) {
        return Err(ErrorUnauthorized("Please authenticate!"));
    }

    // Search for the matching music in the list
    let list = config
        .list
        .get_list()
        .map_err(|_| F("Could not get the list of musics"))?;
    let music = list
        .iter()
        .find(|f| f.pretty_filename().eq(&query.filename))
        .ok_or_else(|| ErrorNotFound("Music not found!"))?;

    Ok(
        actix_files::NamedFile::open(config.list.get_music_file_path(music))?
            .set_content_type(mime::Mime::from_str("audio/mpeg").unwrap()),
    )
}
