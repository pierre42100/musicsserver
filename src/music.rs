use std::error::Error;

/// Single music object
use json::object;

#[derive(Debug)]
pub struct Music {
    pub id: usize,
    pub title: String,
    pub artist: String,
    pub file_name: String,
}

impl Music {
    /// Create a new empty Music object
    pub fn empty() -> Music {
        Music {
            id: 0,
            title: "".to_string(),
            artist: "".to_string(),
            file_name: "".to_string(),
        }
    }

    /// Generate a new entry from a Json value
    pub fn from_json(j: &json::JsonValue) -> Result<Music, Box<dyn Error>> {
        Ok(Music {
            id: j["id"].as_usize().ok_or("id missing!")?,
            title: j["title"].as_str().ok_or("title missing!")?.to_string(),
            artist: j["artist"].as_str().ok_or("artist missing!")?.to_string(),
            file_name: j["file_name"]
                .as_str()
                .ok_or("file_name missing!")?
                .to_string(),
        })
    }

    /// Check whether the current music object is valid or not
    pub fn is_valid(&self) -> bool {
        self.id > 0
            && !self.title.is_empty()
            && !self.artist.is_empty()
            && !self.file_name.is_empty()
    }

    pub fn pretty_filename(&self) -> String {
        format!("{} - {}.mp3", self.artist, self.title).replace('/', "-")
    }
}

impl std::convert::From<&Music> for json::JsonValue {
    fn from(music: &Music) -> Self {
        object! {
            "id" => music.id,
            "title" => music.title.to_string(),
            "artist" => music.artist.to_string(),
            "file_name" => music.file_name.to_string()
        }
    }
}
