// Dependencies
extern crate json;
extern crate yaml_rust;

// Modules
mod config;
pub mod music;
pub mod musics_list;
pub mod server;
#[allow(clippy::needless_late_init)]
#[allow(unused_must_use)]
mod webdav;

// Re-exports
pub use config::Config;
pub use music::Music;
pub use musics_list::MusicsList;
pub use server as Server;
