use std::error::Error;
use std::fmt::Display;
use std::fs;
use std::path::{Path, PathBuf};

use yaml_rust::YamlLoader;

#[derive(Debug, Clone)]
pub struct Client {
    pub id: String,
    pub token: String,
    pub origin: Option<String>,
    pub read_only: bool,
}

#[derive(Debug)]
pub struct Config {
    pub clients: Vec<Client>,
    pub server_port: u16,
    pub server_address: String,
    pub storage: PathBuf,
    pub base_url: String,
}

impl Config {
    fn parse_string_val<D: Display>(val: D) -> String {
        let val = val.to_string();
        if let Some(stripped) = val.strip_prefix('$') {
            std::env::var(stripped).unwrap()
        } else {
            val
        }
    }

    /// Load the configuration
    pub fn load(path: &str) -> Result<Config, Box<dyn Error>> {
        // Read file content
        let content = fs::read_to_string(path)?;

        // Parse YAML
        let docs = YamlLoader::load_from_str(&content)?;
        let doc = &docs[0];

        // Clients tokens
        let mut clients = vec![];

        for (id, client_info) in doc["clients"].as_hash().ok_or("clients missing!")? {
            clients.push(Client {
                id: Self::parse_string_val(id.as_str().ok_or("client->id missing!")?),
                token: Self::parse_string_val(
                    client_info["token"]
                        .as_str()
                        .ok_or("client->token missing")?,
                ),
                origin: client_info["origin"].as_str().map(Self::parse_string_val),
                read_only: client_info["read_only"]
                    .as_bool()
                    .ok_or("client->readOnly missing")?,
            });
        }

        // General configurations
        let conf = Config {
            clients,
            server_port: doc["server"]["port"]
                .as_i64()
                .ok_or("server->address is missing!")? as u16,

            server_address: Self::parse_string_val(
                doc["server"]["address"]
                    .as_str()
                    .ok_or("server->address should be a string!")?,
            ),

            storage: Path::new(&Self::parse_string_val(
                doc["data"]["storage"]
                    .as_str()
                    .ok_or("data->storage should be a string!")?,
            ))
            .to_path_buf(),

            base_url: Self::parse_string_val(
                doc["base_url"]
                    .as_str()
                    .ok_or("base_url should be a string!")?,
            ),
        };

        Ok(conf)
    }

    pub fn listen_address(&self) -> String {
        format!("{}:{}", self.server_address, self.server_port)
    }

    pub fn list_path(&self) -> PathBuf {
        self.storage.join("index.json")
    }

    /// Attempt to find a client by its access token
    pub fn find_client_by_token(&self, token: &str) -> Option<&Client> {
        self.clients.iter().find(|&client| client.token.eq(token))
    }

    /// Search a client with an allowed origin
    pub fn any_client_allow_origin(&self, origin: &str) -> bool {
        for c in &self.clients {
            if let Some(o) = &c.origin {
                if origin.eq(o) {
                    return true;
                }
            }
        }

        false
    }
}
