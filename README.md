# Musics Server
Basic REST API that aims to store and serve a music library.

A Webdav access is available at `/dav/`, which should work properly with `davfs2`